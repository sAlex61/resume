# Resume
Резюме в файле Resume(ru).docx.

## Certificates
В папке Certificates сертификаты пройденных курсов и интенсивов, а также все исходные коды выполненных задач.

## Test Project
В папке Test Project тестовый проект, который находится на стадии разработки. Инструменты: asp.net core, EF - backend; vue js, naxt js, bootstrap, font-awesome - frontend.
